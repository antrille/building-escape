// Fill out your copyright notice in the Description page of Project Settings.

//#include "DrawDebugHelpers.h"
#include "Grabber.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Components/PrimitiveComponent.h"

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	const auto OwnerName = GetOwner()->GetName();

	UE_LOG(LogTemp, Warning, TEXT("Grabber is attached to %s"), *OwnerName);

	if (! FindPlayerController())
	{		
		UE_LOG(LogTemp, Error, TEXT("Grabber: %s PlayerController is not found!"), *OwnerName);
	}

	if (! FindPhysicsHandleComponent())
	{
		UE_LOG(LogTemp, Error, TEXT("Grabber: %s missing PhysicsHandle component!"), *OwnerName);
	}

	if (! SetupInputComponent())
	{
		UE_LOG(LogTemp, Error, TEXT("Grabber: %s missing InputComponent"), *OwnerName);
	}
}

// Get attached Physics Handle component
bool UGrabber::FindPhysicsHandleComponent()
{
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();

	return PhysicsHandle != nullptr;
}

bool UGrabber::FindPlayerController()
{
	PlayerController = GetWorld()->GetFirstPlayerController();

	return PlayerController != nullptr;
}

// Setup InputComponent (only appears at runtime)
bool UGrabber::SetupInputComponent()
{
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();

	if (InputComponent == nullptr)
	{
		return false;
	}

	InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::ToggleGrab);

	return true;
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UGrabber::ToggleGrab()
{
	if (! PhysicsHandle)
	{
		return;
	}

	// If player has something grabbed atm -- release it
	if (PhysicsHandle->GrabbedComponent)
	{
		PhysicsHandle->ReleaseComponent();
		return;
	}

	const auto Hit = GetFirstPhysicsBodyInReach();
	const auto Actor = Hit.GetActor();

	if (! Actor)
	{
		return;
	}

	const auto ComponentToGrab = Hit.GetComponent();
	
	if (! ComponentToGrab)
	{
		return;
	}

	PhysicsHandle->GrabComponentAtLocationWithRotation(
		ComponentToGrab,
		NAME_None,
		ComponentToGrab->GetOwner()->GetActorLocation(),
		ComponentToGrab->GetOwner()->GetActorRotation()
	);
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (PhysicsHandle && PhysicsHandle->GrabbedComponent)
	{
		PhysicsHandle->SetTargetLocation(GetTraceLineEnd());
	}
}

// Get player view trace line
FVector UGrabber::GetTraceLineEnd() const
{
	FVector Location;
	FRotator Direction;
	PlayerController->GetPlayerViewPoint(Location, Direction);

	return Location + Direction.Vector() * ReachDistance;
}

FHitResult UGrabber::GetFirstPhysicsBodyInReach() const
{
	const auto Pawn = GetOwner();
	const auto World = GetWorld();
	const auto Location = Pawn->GetActorLocation();
	const auto LineTraceEnd = GetTraceLineEnd();

	/// Get hit result from the line tracing
	FHitResult Hit;
	World->LineTraceSingleByObjectType(
		Hit,
		Location,
		LineTraceEnd,
		FCollisionObjectQueryParams(ECC_PhysicsBody),
		FCollisionQueryParams(NAME_None, false, Pawn)
	);

	return Hit;
}