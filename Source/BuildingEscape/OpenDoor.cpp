// Fill out your copyright notice in the Description page of Project Settings.

#include "OpenDoor.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Components/PrimitiveComponent.h"

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	Owner = GetOwner();
}

// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!PressurePlate) 
	{
		return;
	}
		
	if (GetPressurePlateTotalMass() >= TriggerMass)
	{
		OnOpen.Broadcast();
	} 
	else
	{
		OnClose.Broadcast();
	}
}

float UOpenDoor::GetPressurePlateTotalMass() const
{
	if (! PressurePlate)
	{
		return 0;
	}

	TArray<AActor*> OverlappingActors;
	auto TotalMass = 0.f;

	PressurePlate->GetOverlappingActors(OverlappingActors);

	for (const auto* Actor : OverlappingActors)
	{
		UPrimitiveComponent* Primitive = Actor->FindComponentByClass<UPrimitiveComponent>();

		if (! Primitive)
		{
			continue;
		}
		
		TotalMass += Primitive->GetMass();
	}

	return TotalMass;
}
